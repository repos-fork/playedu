<p align="center">
<img src="https://meedu.cloud.oss.meedu.vip/playedu/%E5%A4%B4%E5%9B%BE.jpg"/>
</p>

## 系统介绍

PlayEdu是由白书科技团队经营多年线上教培领域打造出的一款业内领先的线上培训解决方案。PlayEdu基于Java + MySQL开发，采用前后端分离模式，前端核心框架为React18，后端核心框架为SpringBoot3。开源版本提供部门管理、学员管理、在线视频学习、学员进度追踪、视频私有化存储等基础培训功能。  
**针对企业级培训场景，我们精心打造了“功能更多、响应更快、并发更强”的企业版本，满足企业多样化的培训需求。企业版本支持音视频学习、文档在线预览、线上考试、学习任务等多种学习方式，并提供多重安全防护，如视频转码加密、防盗链、学习防快进、防挂机等。同时，我们集成了企业微信、钉钉、飞书等主流办公系统，帮助企业快速部署专属培训平台！**

## 常用链接

| 站点       | 链接                                                                         |
| ---------- | ---------------------------------------------------------------------------- |
| 官网       | [https://www.playeduos.com](https://www.playeduos.com)                             |
| **企业版** | [https://www.playeduos.com/function.html](https://www.playeduos.com/function.html)     |
| 部署文档   | [https://faq.playeduos.com/opensource-maintenance-handbook/article/t08o2iHfLR](https://faq.playeduos.com/opensource-maintenance-handbook/article/t08o2iHfLR)                 |
| 系统演示   | [https://www.playeduos.com/demo.html](https://www.playeduos.com/demo.html)                 |
| 问答社区   | [https://faq.playeduos.com/qa?scene=new](https://faq.playeduos.com/qa?scene=new) |

## 依赖前端项目

- [后台界面程序](https://github.com/PlayEdu/backend)
- [PC 界面程序](https://github.com/PlayEdu/frontend)
- [H5 界面程序](https://github.com/PlayEdu/h5)

## 界面预览

![学员端口界面预览](https://meedu.cloud.oss.meedu.vip/playedu/%E5%89%8D%E5%8F%B0%E9%A1%B5%E9%9D%A2.jpg)

![管理后台界面预览](https://meedu.cloud.oss.meedu.vip/playedu/%E5%90%8E%E5%8F%B0%E9%A1%B5%E9%9D%A2.jpg)

## 使用协议

● 要求

- 保留页脚处版权信息。
- 保留源代码中的协议。
- 如果修改了代码，则必须在文件中进行说明。